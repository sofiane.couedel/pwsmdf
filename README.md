# Projet_Web_Semantique_Maires_De_France

## Dataset

A l'origine de notre projet nous avions décidé de partir sur un Dataset de musique a l'image de Spotify. Cepdendant, et ceux, même après avoir trouvé une bonne dataset pour ce sujet, nous avns décider de de partir sur une dataset de tous les maires de frances.
Ainsi, nous avons des données avant le mandat en cours (mandat de 2014) et celles du mandat actuel (depuis 2020). 
Pour cela, nous sommes aller chercher sur le site officel des dataset du gouvernement français 
* [data.gouv](https://www.data.gouv.fr/). 

Nous avons donc trouvé les mandats de 
* [2019](https://www.data.gouv.fr/fr/datasets/r/896bb8a4-aa9f-4025-823d-1372832d02db) (soit avant les élections de 2020) 
et les mandats de 
* [2021](https://www.data.gouv.fr/fr/datasets/r/2876a346-d50c-4911-934e-19ee07b0e503).

Ainsi, nous allons vous présenter des requetes comparant la partité, les milieux socio-professionnel et les installations des villes dans lesquelles les maires exercent. 

## TARQL

Pour sémantifier nos données, nous avons utilisé le projet [TARQL](http://tarql.github.io/).
```
$ cd Construct_Output/tarql-1.2/bin
$ ./tarql CONSTRUCT_NEW.rq maires-2019.csv > output/maires-2019.ttl
$ ./tarql CONSTRUCT_NEW.rq maires-2020.csv > output/maires-2020.ttl 
```

## Authors and acknowledgment
Sofiane Couedel & Lou-Anne Sauvetre

## License
For open source projects, say how it is licensed.
